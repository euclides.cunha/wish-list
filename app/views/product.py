import inject
from http import HTTPStatus
import logging

from flask import json
from webargs.flaskparser import use_args

from app.config import ApplicationContext, ProductAPI
from src.main.core.common.errors import ServiceNotAvailable, ResourceNotFound
from src.main.schema.product import ProductRequest
from src.main.core.resources.product import create, list
from app.urls import basic_auth

logger = logging.getLogger(__name__)


@inject.params(context=ApplicationContext, product_service=ProductAPI)
@use_args(ProductRequest)
@basic_auth.required
def product_request(parsed, context, product_service):
    try:
        if len(list(**{"customer_id": parsed["customer_id"], "product_id": parsed["product_id"]})) > 0:
            return json.dumps({"msg": "Product wish already saved"}), HTTPStatus.BAD_REQUEST

        meta_data, http_status = product_service.get_product_by_id(parsed["product_id"])

        if http_status == HTTPStatus.OK and len(meta_data) > 0:
            parsed["meta_data"] = meta_data
            product = create(parsed)
            return json.dumps(product)
        else:
            return json.dumps({"msg": "Product doesn't exist"}), HTTPStatus.BAD_REQUEST
    except ResourceNotFound:
        return json.dumps({"msg": "Product doesn't exist"}), HTTPStatus.NOT_FOUND
    except ServiceNotAvailable:
        return json.dumps({"msg": "Service not avaible"}),  HTTPStatus.INTERNAL_SERVER_ERROR


@inject.params(context=ApplicationContext, product_service=ProductAPI)
@basic_auth.required
def product_list(customer_id, context, product_service):
    query = {"customer_id": customer_id}
    product = list(**query)
    return json.dumps(product)
