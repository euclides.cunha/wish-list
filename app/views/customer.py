from http import HTTPStatus
import inject
from bson import ObjectId
from flask import request, json
from webargs.flaskparser import use_args

from app.config import ApplicationContext
from src.main.schema.customer import CustomerRequest, CustomerUpdate
from src.main.core.resources.customer import list, create, update, delete
from src.main.core.resources.product import list as product_list, delete as product_delete
from app.urls import basic_auth

import logging

logger = logging.getLogger(__name__)


def healthcheck():
    return "OK"


@inject.params(context=ApplicationContext)
@basic_auth.required
def customer_list(context, customer_id=None):
    query = {}
    if customer_id:
        query["id"] = customer_id
    customers = list(**query)

    for customer in customers:
        customer["product_list"] = product_list(**{"customer_id": str(customer["_id"]["$oid"])})

    return json.dumps(customers)


@use_args(CustomerRequest)
@inject.params(context=ApplicationContext)
@basic_auth.required
def customer_request(parsed, context):
    if len(list(**{"email": parsed["email"]})) > 0:
        return json.dumps({"msg": "email already in use"}), HTTPStatus.BAD_REQUEST

    customer = create(parsed)
    return json.dumps(customer)


@use_args(CustomerUpdate)
@inject.params(context=ApplicationContext)
@basic_auth.required
def customer_update(parsed, customer_id, context):
    if customer_id:
        if request.method == 'POST':
            query = {"id": ObjectId(customer_id)}
            update(parsed, **query)
            return json.dumps({"msg": "OK"})
        elif request.method == 'DELETE':
            product_delete(**{"customer_id": customer_id})

            delete(**{"id": ObjectId(customer_id)})
            return json.dumps({"msg": "OK"})

