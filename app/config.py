from src.main.core.helpers.BaseClient import BaseClient


class ApplicationContext:
    pass


class db:
    pass


class ProductAPI(BaseClient):

    def __init__(self, app):
        self._base_product_uri = app.config.PRODUCT_URI

        super().__init__(self._base_product_uri)