from flask import Flask
from flask_basicauth import BasicAuth

app = Flask(__name__)
# This should be not here
app.config['BASIC_AUTH_USERNAME'] = 'user'
app.config['BASIC_AUTH_PASSWORD'] = 'password'

basic_auth = BasicAuth(app)

from app.views.customer import customer_request, customer_update, customer_list, healthcheck
from app.views.product import product_list, product_request

app.add_url_rule('/healthcheck', methods=['GET'], endpoint='healthcheck', view_func=healthcheck, )
app.add_url_rule('/customer', methods=['GET'], endpoint='customer_list', view_func=customer_list)
app.add_url_rule('/customer/<customer_id>', methods=['GET'], endpoint='customer_by_id', view_func=customer_list)
app.add_url_rule('/customer', methods=['POST'], endpoint='customer_create', view_func=customer_request)
app.add_url_rule('/customer/<customer_id>', methods=['POST','DELETE'], endpoint='customer_update', view_func=customer_update)
app.add_url_rule('/product/<customer_id>', methods=['GET'], endpoint='product_list', view_func=product_list)
app.add_url_rule('/product', methods=['POST'], endpoint='product_request', view_func=product_request)

