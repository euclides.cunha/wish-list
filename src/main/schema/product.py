from marshmallow import Schema, fields


class ProductRequest(Schema):
    customer_id = fields.String(required=True)
    product_id = fields.String(required=True)


class ProductList(Schema):
    customer_id = fields.String(required=True)
