from marshmallow import Schema, fields


class CustomerRequest(Schema):
    name = fields.String(required=True)
    email = fields.String(required=True)


class CustomerUpdate(Schema):
    name = fields.String(required=False)
    email = fields.String(required=False)
