from flask import json

from app.config import ProductAPI


class ProductService(ProductAPI):

    def __init__(self, app_context):
        super(ProductService, self).__init__(app_context)
        self.__timeout = 20

    def get_product_by_id(self, product_id, **kwargs):
        endpoint = "{}{}".format(self._base_product_uri, product_id)

        response_stream, http_status_code = self._perform_client_get(endpoint, timeout=self.__timeout)

        return response_stream, http_status_code

    def get_product_by_page(self, page, **kwargs):
        endpoint = "{}?PAGE={}".format(self._base_product_uri, page)

        response_stream, http_status_code = self._perform_client_get(endpoint, timeout=self.__timeout)

        return response_stream, http_status_code

