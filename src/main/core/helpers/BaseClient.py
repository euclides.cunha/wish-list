import time
from enum import Enum
from http import HTTPStatus
import requests
import logging

from flask import json

from src.main.core.common.errors import ResourceNotFound, ServiceNotAvailable

logger = logging.getLogger(__name__)


class BaseClient:
    resources = {}

    def __init__(self, base_url=None):
        self.__base_url = base_url

    def get_resource(self, name, params: dict):
        resource = self.resources[name].format(**params)
        full_resource = self.__base_url + resource

        return full_resource

    def __perform_client_method(self, method, url, **kargs):
        client = requests
        logger.debug("creating _client: {} for get".format(client))
        timeout = kargs['timeout'] if 'timeout' in kargs else 30

        logger.info("resource:{} method:{} with timeout:{}".format(url, method, timeout))
        try:

            ts = time.time()
            response = requests.request(method, url, **kargs)
            te = time.time()

            logger.info("resource: {} response status: {} - {:.02f}ms".format(url, response.status_code, (te - ts) * 1000))

            body = response.text
            logger.debug("resouce: {} response body: {}".format(url, body))

            if response.status_code == 404:
                logger.info("resource: {} not found".format(url))
                raise ResourceNotFound("resource {} does not found.".format(url), body)
            if response.status_code == 500:
                logger.error("resource: {} is not available".format(url))
                raise ServiceNotAvailable("resource {} is not available.".format(url))
        except requests.exceptions.ConnectTimeout as timeout_error:
            logger.error("resource:{} timeout error {}".format(url, timeout))
            raise timeout_error
        finally:
            logger.debug("closing _client:{}".format(client))

        return json.loads(body), HTTPStatus(response.status_code)

    def _perform_client_get(self, url, timeout=30, **kargs):
        return self.__perform_client_method(HTTPMethod.GET.value, url, timeout=timeout, **kargs)

    def _perform_client_post(self, url, data, timeout=30, **kargs):
        return self.__perform_client_method(HTTPMethod.POST.value, url, data=data, timeout=timeout, **kargs)

    def _perform_client_delete(self, url, timeout=30, **kargs):
        return self.__perform_client_method(HTTPMethod.DELETE.value, url, timeout=timeout, **kargs)


class HTTPMethod(str, Enum):
    GET = 'GET'
    POST = 'POST'
    PUT = 'PUT'
    DELETE = 'DELETE'
    HEAD = 'HEAD'
    CONNECT = 'CONNECT'
    OPTIONS = 'OPTIONS'
    PATCH = 'PATCH'
    TRACE = 'TRACE'
