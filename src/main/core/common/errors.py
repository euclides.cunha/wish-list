

class ServiceError(Exception):
    pass


class ServiceNotAvailable(ServiceError):
    pass


class ResourceNotFound(ServiceError):
    pass
