from flask import json

from src.main.core.models.model import Product

def create(fields):

    product = Product()
    product.customer_id = fields["customer_id"]
    product.product_id = fields["product_id"]
    product.meta_data = fields["meta_data"]
    product.save()

    return json.loads(product.to_json())


def delete(**kwargs):
    products = Product().get(**kwargs)
    for product in products:
        product.delete()


def list(**kwargs):
    products = Product().get(**kwargs)

    return json.loads(products.to_json())
