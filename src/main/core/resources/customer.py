from flask import json


from src.main.core.models.model import Customer


def list(**kwargs):
    customer = Customer().get(**kwargs)

    return json.loads(customer.to_json())


def create(fields):
    customer = Customer()
    customer.name = fields["name"]
    customer.email = fields["email"]
    customer.save()

    return json.loads(customer.to_json())


def update(fields, **kwargs):
    customer = Customer().get_one(**kwargs)
    customer.update(**fields)


def delete(**kwargs):
    customer = Customer().get_one(**kwargs)
    customer.delete()

