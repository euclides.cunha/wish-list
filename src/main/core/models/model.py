from mongoengine import Document, StringField, DictField


class Customer(Document):
    """
    Customer Model
    """
    meta = {'db_alias': 'product_wish_db'}

    name = StringField(required=True, max_length=200)
    email = StringField(required=True, max_length=200)

    def save(self, *args, **kwargs):
        return super(Customer, self).save(*args, **kwargs)

    def get(self, **kwargs):
        return Customer.objects(**kwargs)

    def get_one(self, **kwargs):
        return self.get(**kwargs).first()


class Product(Document):
    """
    Product Model
    """
    meta = {'db_alias': 'product_wish_db'}

    customer_id = StringField(required=True)
    product_id = StringField(required=True)
    meta_data = DictField(required=True)

    def save(self, *args, **kwargs):
        return super(Product, self).save(*args, **kwargs)

    def get(self, **kwargs):
        return Product.objects(**kwargs)
