# Wish List
Writing an application that handle a products list 

### Enviroment config and authentication
```
Is important use an api address in environment variable "PRODUCT_URI"
Create a database called "product_wish"
Necessary use basic authentication, username: user, password: password
```

### Start application 
This application depends mongodb
```
make setup
make start
```

### Testing
```
make unit
```

#### Running as docker compose
```
sudo docker-compose --verbose build
sudo docker-compose --verbose up
```
